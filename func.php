<?php
require_once 'connect.php';





// Create

// if (isset($_POST['submit'])) {
// 	$sql = ("INSERT INTO `users`(`name`, `last_name`, `pos`) VALUES(?,?,?)");
// 	$query = $pdo->prepare($sql);
// 	$query->execute([$name, $last_name, $pos]);
// 	$success = '<div class="alert alert-success alert-dismissible fade show" role="alert">
//   <strong>Данные успешно отправлены!</strong> Вы можете закрыть это сообщение.
//   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
//     <span aria-hidden="true">&times;</span>
//   </button>
// </div>';

// }

// Read

$sql = "SELECT * FROM `registration_to_course`";
$result = $conn->query($sql);
$rows = $result->fetch_all(MYSQLI_ASSOC);

// Update
if (isset($_POST['edit-submit'])) {
    $edit_surname = $_POST['edit_surname'];
$edit_firstName = $_POST['edit_firstname'];
$edit_middleName = $_POST['edit_middlename'];
$edit_email = $_POST['edit_email'];
$edit_number = $_POST['edit_number'];
$edit_courses = $_POST['edit_courses'];
$edit_reviewed = $_POST['edit_reviewed'];
echo '--------------->'.$edit_reviewed ;
$edit_reviewtime = $_POST['edit_reviewtime'];
$get_id = $_GET['id'];
    // $sqll = "UPDATE registration_to_course SET Surname=?, FirstName=?,MiddleName=?,Email=?,Number=?,Courses=? WHERE id=?";
    // $querys = $pdo->prepare($sqll);
    // $querys->execute([$edit_surname, $edit_firstName, $edit_middleName, $edit_email,$edit_number,$edit_courses,$get_id]);
    // header('Location: '. $_SERVER['HTTP_REFERER']);
    $sql = "UPDATE registration_to_course SET Surname=?, FirstName=?, MiddleName=?, Email=?, Number=?, Courses=? ,Reviewed = ?,ReviewTime=? WHERE id=?";
    $query = $conn->prepare($sql);

    // Привязка параметров
    $query->bind_param("ssssisssi", $edit_surname, $edit_firstName, $edit_middleName, $edit_email, $edit_number, $edit_courses,$edit_reviewed,$edit_reviewtime, $get_id);

    $query->execute();
    header('Location: ' . $_SERVER['HTTP_REFERER']);
}

// DELETE
if (isset($_POST['delete_submit'])) {
    $get_id = $_GET['id'];
    // $sql = "DELETE FROM registration_to_course WHERE id=?";
    // $query = $pdo->prepare($sql);
    // $query->execute([$get_id]);
    // header('Location: ' . $_SERVER['HTTP_REFERER']);
    $sql = "DELETE FROM registration_to_course WHERE id=?";
    $query = $conn->prepare($sql);

    // Привязка параметров
    $query->bind_param("i", $get_id);

    $query->execute();
    header('Location: ' . $_SERVER['HTTP_REFERER']);

    
}
// check
if (isset($_POST['yes_submit'])) {
    $get_id = $_GET['id'];
    $check_reviewed = 1;
    $check_reviewtime = date("Y-m-d H:i:s");
    // $sql = "DELETE FROM registration_to_course WHERE id=?";
    // $query = $pdo->prepare($sql);
    // $query->execute([$get_id]);
    // header('Location: ' . $_SERVER['HTTP_REFERER']);
    $sql = "UPDATE registration_to_course SET Reviewed = ?,ReviewTime=? WHERE id=?";
    $query = $conn->prepare($sql);

    // Привязка параметров
   $query->bind_param("ssi", $check_reviewed,$check_reviewtime, $get_id);

    $query->execute();
    header('Location: ' . $_SERVER['HTTP_REFERER']);
}