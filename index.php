<?php
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>DPA-Training Center</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
</head>

<body>
  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">

      <h1 class="logo me-auto">
        <a href="index.php">
          <h5>
            Учебный центр <br>Государственного агентства по защите персональных данных <br>при Кабинете Министров Кыргызской Республики
          </h5>
        </a>
      </h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo me-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">Главная</a></li>
          <li><a class="nav-link scrollto" href="#why-us">О нас</a></li>
          <li><a class="nav-link scrollto" href="#services">Курсы</a></li>
          <li><a class="nav-link scrollto" href="#contact">Контакты</a></li>
          <li><a class="nav-link" href="News.php">Новости</a></li>
          <li><a href="https://www.facebook.com/okuudpa.kg" class="facebook"><i class="bx bxl-facebook"></i></a></li>
          <li> <a href="https://instagram.com/okuudpa.kg?igshid=MmU2YjMzNjRlOQ==" class="instagram"><i class="bx bxl-instagram"></i></a></li>
          <li> <a href=" https://t.me/educenterDPA" class="telegram"><i class="bx bxl-telegram"></i></a></li>
          <!-- <li><a class="getstarted scrollto" href="#about">Связаться с нами</a></li> -->
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->
    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
          <h2>Добро пожаловать в наш учебный центр!<br>Мы рады предложить вам курсы, специально разработанные для повышения осведомленности в сфере персональных данных, кибербезопасности, цифровой гигиены и цифровых навыков сотрудников государственных служб, компаний а также граждан. </h2>
          <div class="d-flex justify-content-center justify-content-lg-start">
            <a href="#services" class="btn-get-started scrollto">Записаться на курсы</a>
          </div>
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
          <img src="assets/img/hero-img.png" class="img-fluid animated" alt="">
        </div>
      </div>
    </div>
  </section><!-- End Hero -->

  <main id="main">
    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us section-bg">
      <div class="container-fluid" data-aos="fade-up">

        <div class="row">

          <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">
            <div class="about-us">
              <div class="content">
                <h3>О<strong> нас</strong></h3>
                <ul>
                  <li> <a href="assets/documents/Privacy_Policy_watermark_watermarked.pdf">Политика конфиденциальности</a></li>
                  <!-- <li> <a href="assets/documents/Service_Agreement.pdf">Договор об оказании услуг</a></li> -->
                  <li> <a href="assets/documents/license.pdf">Лицензия</a></li>
                  <li> <a href="assets/documents/position.pdf">Положение</a></li>
                  <li> <a href="assets/documents/ordinance.pdf">Постановление</a></li>
                </ul>

                <h4><strong>Формы договоров:</strong></h4>
                <ul>
                  <li><a href="assets\documents\Договор_об_оказании_услуг_государственным_органам.docx">Договор об оказании услуг государственным органам </a></li>
                  <li><a href="assets\documents\Договор_об_оказании_услуг_юридическим_лицам.docx"> Договор об оказании услуг юридическим лицам</a></li>
                  <li><a href="assets\documents\Договор_об_оказании_услуг_физическим_лицам.docx">Договор об оказании услуг физическим лицам </a></li>
                </ul>
              </div>
            </div>
            <div class="content">
              <h3>Почему вам стоит выбрать <strong>наши курсы</strong></h3>
              <p>
                Наш учебный центр предлагает комплексное обучение в сфере цифрового направления, которое поможет участникам развить свои навыки и компетенции в этой области
              </p>
            </div>

            <div class="accordion-list">
              <ul>
                <li>
                  <a data-bs-toggle="collapse" class="collapse" data-bs-target="#accordion-list-1"><span>01</span>Чему вы научитесь на наших курсах? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-1" class="collapse show" data-bs-parent=".accordion-list">
                    <p>
                      <strong> Знание законодательства о персональных данных а также работа с ними:</strong> Понимание особенностей международного и кыргызского законодательства в области персональных данных. Включает знание требований, ограничений и правил, установленных GDPR и Закона “Об информации персонального характера”. Приобретение навыков по обработке, хранению и передаче персональных данных в соответствии с требованиями Законодательства Кыргызской Республики. Включает знание основных принципов и правил работы с персональными данными, включая согласование, безопасность и конфиденциальность.

                      <br><strong>Осведомленность о digital-трендах и технологиях:</strong> Понимание современных цифровых трендов и технологий, их преимуществ и недостатков в контексте юридической сферы. Включает знание основных аспектов цифровой трансформации, электронной коммерции, облачных технологий и других современных инструментов.

                      <br><strong>Информационная кибербезопасность:</strong> Основы защиты информации и данных, как на личном, так и на корпоративном уровне. Включает понимание угроз информационной безопасности, методов предотвращения и выявления инцидентов, а также основные принципы кибербезопасности.

                      <br> Эти навыки помогут слушателю курсов стать компетентным специалистом в области работы с персональными данными, а также эффективно справляться с задачами по обеспечению безопасности и соблюдению законодательства в цифровой среде.

                    </p>
                  </div>
                </li>

                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-2" class="collapsed"><span>02</span>Кому подойдут наши курсы? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-2" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      <br> <strong> Всем желающим гражданам.</strong>
                      <br>В быстро развивающемся мире технологий все больше распространяются киберпреступления и угрозы в онлайн-среде. Знание основ защиты персональных данных, кибербезопасности а также цифровых навыков поможет гражданам использовать интернет безопасно, избежать кибератаки, кражи личных данных и мошенничества в цифровом мире.
                      <br><strong> Государственным служащим</strong>

                      <br>В государственных сферах возникает потребность в новых знаниях у сотрудников в связи с цифровой трансформацией. Они должны обладать способностью быстро и эффективно обрабатывать большие объемы данных, знать основы законодательства.


                      <br><strong> Юристам в разных компаниях</strong>
                      <br>Юристы компаний,в том числе консалтинговых нуждаются в новых знаниях, чтобы обеспечить защиту и безопасность при покупке и продаже данных клиентов.


                      <br><strong> Техническим специалистам (IT-специалистам)</strong>
                      <br>Техническим специалистам, отвечающим за информационную безопасность необходимо постоянное развитие а также обучение новым методикам определения угроз для полноценной защиты конфиденциальной информации, обеспечение ее целостности при полном отсутствии риска нанести ущерб работе предприятия.


                      <br><strong> Владельцам бизнеса</strong>
                      <br>Киберпреступления, такие как кибератаки, фишинговые атаки и вредоносные программы, представляют серьезные угрозы для бизнеса. Знание основ кибербезопасности позволяет владельцам бизнеса принимать меры для защиты своих информационных систем, клиентских данных и финансовых ресурсов. Законодательство о защите данных становится все более строгим, и компании обязаны соблюдать определенные стандарты безопасности и защиты персональных данных своих клиентов. Знание правил и требований в этой области позволяет владельцам бизнеса избежать нарушений и юридических проблем.

                    </p>
                  </div>
                </li>

                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-3" class="collapsed"><span>03</span>Формат курса/График курсов<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-3" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      Курсы проводится оффлайн. Продолжительность курса от двух недель до шести месяцев (в зависимости от выбранного курса и уровня), 72 академических часа. Количество участников в одной группе от 10 до 17 студентов
                      <br><a href="assets/documents/Schedule_of_teachers_for_courses.pdf" target="_blank">График курсов</a>
                    </p>
                  </div>
                </li>
                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-4" class="collapsed"><span>04</span>Оплата/Реквизиты<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-4" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      Стоимость курса указана в разделе “Наши курсы”. Оплату можно производить через банк и платежные системы
                      <br>
                      <br> <strong>Реквизиты для оплаты курсов</strong>
                      <br> <strong> ИНН:</strong> 01001202210023
                      <br> <strong>Расчетный счет:</strong>Р 4402031102004184
                      <br> <strong>БИК:</strong> 440001
                      <br> <strong> Банк:</strong> Центральное казначейство при МФ КР
                      <br> <strong>ОКПО: </strong> 31296230
                      <br> <strong>Код платежа:</strong> 14232500
                    </p>
                  </div>
                </li>

                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-5" class="collapsed"><span>05</span>Как проходит обучение?<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-5" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      <br>
                      <strong> Регистрация:</strong>Слушателю необходимо зарегистрироваться удобным для него способом. Для этого ему необходимо заполнить регистрационную форму, указав свои личные данные, контактную информацию и выбрать интересующий курс.

                      <br> <strong>Выбор курса:</strong> После регистрации пользователь может выбрать нужный ему программу обучения из предложенного списка. Учебный центр предлагает курсы по различным тематикам, с которыми можете ознакомиться ниже.
                      <br><strong>Оплата:</strong> После выбора курса слушатель должен осуществить оплату обучения. Учебный центр предоставляет различные способы оплаты, После успешной оплаты пользователь получает подтверждение о регистрации на курс.
                      <br><strong>Обучение:</strong> Слушатель уведомляется о дате и времени начала курсов. Курсы будут проходить оффлайн по адресу г. Бишкек, пр. Чуй, 265а, здание Национальной Академии наук, 2 этаж, западное крыло.
                      Курс дает вам возможность просмотр лекций в виде презентаций, выполнение практических заданий, прохождение тестов и участие в обсуждениях с преподавателями и другими участниками курса.
                      <br><strong>Доступ к материалам:</strong> После подтверждения регистрации слушателю предоставляется доступ к учебным материалам на платформе Moodle, где вы можете просматривать лекции, скачивать учебные пособия и другие материалы, а также взаимодействовать с преподавателями и другими участниками курса.
                      <br><strong>Оценка и сертификация:</strong> По окончании курса пользователь может получить оценку за выполненные работы и, при успешном их прохождении, получить сертификат об окончании обучения. Этот документ может подтверждать полученные знания и навыки

                    </p>
                  </div>
                </li>
                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-6" class="collapsed"><span>06</span>Что вы получите после окончания курса?<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-6" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      <br> <strong> Повышение карьерных возможностей:</strong> Обладая новыми знаниями и навыками, вы станете более конкурентоспособным на рынке труда. Вы сможете претендовать на более высокие должности или перейти в новую область, где востребованы ваши умения.
                      <br> <strong> Практические навыки: </strong>Наши курсы ориентированы на развитие практических навыков, которые вы сможете непосредственно применять в своей работе. Вы будете знакомы с современными инструментами и методиками, используемыми в вашей деятельности.
                      <br> <strong> Поддержка и консультации:</strong> Мы предлагаем поддержку и консультации после окончания курса. Вы сможете задавать вопросы и проконсультироваться с нашей командой экспертов по вопросам, связанным с вашей деятельностью или продолжением образования.
                      <br> <strong> Сертификат:</strong> Вы получите официальный сертификат, подтверждающий успешное завершение курса. Это документ, который можно добавить в своё резюме или предоставить потенциальным работодателям, чтобы подтвердить ваши навыки и компетенции.

                    </p>
                  </div>
                </li>
                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-7" class="collapsed"><span>07</span>Когда начнутся ближайшие курсы?<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-7" class="collapse" data-bs-parent=".accordion-list">
                    <!-- <p>
                      <br> <strong> Повышение карьерных возможностей:</strong> Обладая новыми знаниями и навыками, вы станете более конкурентоспособным на рынке труда. Вы сможете претендовать на более высокие должности или перейти в новую область, где востребованы ваши умения.
                      <br> <strong> Практические навыки: </strong>Наши курсы ориентированы на развитие практических навыков, которые вы сможете непосредственно применять в своей работе. Вы будете знакомы с современными инструментами и методиками, используемыми в вашей деятельности.
                      <br> <strong> Поддержка и консультации:</strong> Мы предлагаем поддержку и консультации после окончания курса. Вы сможете задавать вопросы и проконсультироваться с нашей командой экспертов по вопросам, связанным с вашей деятельностью или продолжением образования.
                      <br> <strong> Сертификат:</strong> Вы получите официальный сертификат, подтверждающий успешное завершение курса. Это документ, который можно добавить в своё резюме или предоставить потенциальным работодателям, чтобы подтвердить ваши навыки и компетенции.

                    </p> -->
                    <br> <strong>
                      <center>График курсов Учебного центра на июль - август</center>
                    </strong>
                    <table class="table">
                      <tr>
                        <th>№</th>
                        <th>
                          <center>Наименование платных услуг, предоставляемых по направлениям</center>
                        </th>
                        <th>График </th>
                        <th>Преподаватель</th>
                        <th>Дата</th>
                      </tr>
                      <tr>
                        <td>1</td>
                        <td>Курсы «Правовые основы защите персональных данных»</td>
                        <td>8:00 - 9:30 <br> (понедельник, вторник, среда, четверг, пятница)</td>
                        <td>Омельченко Клим Сергеевич</td>
                        <td><em class="italic">с</em>  <span class="underline">25 июля</span>по <br><span class="underline">7 августа</span> 2023 года</td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>Курсы «Цифровые навыки» (Excel)

                        </td>
                        <td>10:00 - 11:30 <br>
                          (понедельник, вторник, среда, четверг, пятница)
                        </td>
                        <td>Яковлева Регина Сергеевна</td>
                        <td><em class="italic">с</em> <span class="underline">25 июля</span>по <br><span class="underline">7 августа</span> 2023 года</td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>Курсы «Аналитика данных»</td>
                        <td>12:00 - 13:30 <br>
                          (понедельник, вторник, среда, четверг, пятница)
                        </td>
                        <td>Яковлева Регина Сергеевна</td>
                        <td><em class="italic">с</em> <span class="underline">25 июля</span>по<br><span class="underline">22 сентября</span> 2023 года
                          <br>(2 уровня)
                        </td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td>Курсы «Кибербезопасность»</td>
                        <td>18:30 - 20:00 <br>
                          (вторник, среда, четверг)
                        </td>
                        <td>Губаев Тимур Талгатович</td>
                        <td><em class="italic">с</em> <span class="underline">25 июля</span> по <br><span class="underline">22 января</span> 2024 года
                         <br> (4 уровня)
                        </td>
                      </tr>
                    </table>
                    <br> <strong>
                      <center>График курсов Учебного центра на август-сентябрь</center>
                    </strong>
                    <table class="table">
                      <tr>
                        <th>№</th>
                        <th>
                          <center>Наименование платных услуг, предоставляемых по направлениям</center>
                        </th>
                        <th>График </th>
                        <th>Преподаватель</th>
                        <th>Дата</th>
                      </tr>
                      <tr>
                        <td>1</td>
                        <td>Курсы «Правовые основы защите персональных данных»</td>
                        <td>8:00 - 9:30 <br> (понедельник, вторник, среда, четверг, пятница)</td>
                        <td>Омельченко Клим Сергеевич</td>
                        <td><em class="italic">с</em> <span class="underline">7 августа</span>по <br><span class="underline">18 августа</span> 2023 года</td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>Курсы «Цифровые навыки» (Excel)
                        </td>
                        <td>10:00 - 11:30 <br>
                          (понедельник, вторник, среда, четверг, пятница)
                        </td>
                        <td>Яковлева Регина Сергеевна</td>
                        <td><em class="italic">с</em> <span class="underline">8 августа</span>по<br><span class="underline">21 августа</span> 2023 года</td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>Курсы «Аналитика данных»</td>
                        <td>12:00 - 13:30 <br>
                          (понедельник, вторник, среда, четверг, пятница)
                        </td>
                        <td>Яковлева Регина Сергеевна</td>
                        <td><em class="italic">с</em> <span class="underline">25 сентября </span>по<br><span class="underline">24 ноября </span> 2023 года
                         <br> (2 уровня)

                        </td>
                      </tr>

                    </table>
                  </div>
                </li>
              </ul>
            </div>

          </div>

          <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style='background-image: url("assets/img/why-us.png");' data-aos="zoom-in" data-aos-delay="150">&nbsp;</div>
        </div>

      </div>
    </section><!-- End Why Us Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Наши курсы</h2>
          <p>Наш учебный центр предлагает курсы с самыми передовыми знаниями и методиками, которые проводят квалифицированные преподаватели с лицензией министерства образования.</p>
        </div>

        <div class="row">
          <div class="col-xl-4 col-md-6 d-flex align-items-stretch mt-4 my-3" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <div class="icon"><i class="bx bxl-dribbble"></i></div>
              <h4><a href="course-LegalBasisAndProtectionOfPersonalData.php">Правовые основы защиты <br> персональных данных</a></h4>
              <!-- <p>Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p> -->
            </div>
          </div>

          <div class="col-xl-4 col-md-6 d-flex align-items-stretch mt-4 my-3" data-aos="zoom-in" data-aos-delay="200">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-file"></i></div>
              <h4><a href="courses-cybersecurity.php">Кибербезопасность</a></h4>
              <!-- <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p> -->
            </div>
          </div>

          <div class="col-xl-4 col-md-6 d-flex align-items-stretch mt-4 my-3" data-aos="zoom-in" data-aos-delay="400">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-layer"></i></div>
              <h4><a href="courses-DigitalLaw.php">Цифровое право</a></h4>
              <!-- <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</p> -->
            </div>
          </div>


          <!-- <div class="col-xl-4 col-md-6 d-flex align-items-stretch mt-4 my-3" data-aos="zoom-in" data-aos-delay="400">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-layer"></i></div>
              <h4><a href="">Цифровые навыки</a></h4>
             
            </div>
          </div> -->

          <!-- <div class="col-xl-4 col-md-6 d-flex align-items-stretch mt-4 my-3" data-aos="zoom-in" data-aos-delay="400">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-layer"></i></div>
              <h4><a href="">Программирование</a></h4>
             
            </div>
          </div> -->


          <div class="col-xl-4 col-md-6 d-flex  mt-4  my-3" data-aos="zoom-in" data-aos-delay="400">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-layer"></i></div>
              <h4><a href="courses-dataAnalysis.php">Аналитика данных</a></h4>
              <!-- <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</p> -->
            </div>
          </div>
          <div class="col-xl-4 col-md-6 d-flex  mt-4  my-3" data-aos="zoom-in" data-aos-delay="400">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-layer"></i></div>
              <h4><a href="courses-excel.php">Цифровые навыки(Excel)</a></h4>
              <!-- <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</p> -->
            </div>
          </div>
        </div>

      </div>
    </section><!-- End Services Section -->

    <!-- ======= Cta Section ======= -->
    <section id="cta" class="cta">
      <div class="container" data-aos="zoom-in">

        <div class="row">
          <div class="col-lg-9 text-center text-lg-start">
            <!-- <h3>Узнай глубже и защищай свои персоналыные данные</h3>
            <p> Зарегистрируйтесь на наши курсы и повышайте свои практические и теоритические знания в сфере кибербезопасности</p> -->
          </div>
          <!-- <div class="col-lg-3 cta-btn-container text-center">
            <a class="cta-btn align-middle" href="#">Зарегистрироваться</a>
          </div> -->
        </div>

      </div>
    </section><!-- End Cta Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Свяжитесь с нами</h2>
          <p>У вас еще остались вопросы? Свяжитесь с нами</p>
        </div>

        <div class="row">

          <div class="col d-flex align-items-stretch">
            <div class="info">
              <div class="address">
                <a href="https://2gis.kg/bishkek/firm/70000001019368922?m=74.57975%2C42.878094%2F16%2Fp%2F3.21%2Fr%2F-4.36">
                  <i class="bi bi-geo-alt"></i>
                  <h4>Юридический адрес Учебного центра</h4>
                  <p> 720071, Кыргызская Республика, город Бишкек, проспект Чуй, 265-а.</p>
                </a>
              </div>

              <div class="email">
                <a href="mailto:okuu@dpa.gov.kg">
                  <i class="bi bi-envelope"></i>
                  <h4>Email:</h4>
                  <p>okuu@dpa.gov.kg</p>
                </a>
              </div>

              <div class="phone">

                <i class="bi bi-phone"></i>
                <h4>Call:</h4>
                <p>+(312) 641403</p>
                <p>+996 998 950 850</p>

              </div>
              <div class="telegram">
                <a href="https://t.me/educenterDPA">
                  <i class="bi bi-telegram"></i>
                  <h4>Telegram:</h4>
                  <p>+996 998 950 850</p>
                </a>
              </div>

              <div class="whatsapp">
                <a href="https://wa.me/message/RONQERI2GSSOJ1">
                  <i class="bi bi-whatsapp"></i>
                  <h4>Whatsapp:</h4>
                  <p>+996 998 950 850</p>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Учебный центр Государственного агентства по защите персональных данных при Кабинете Министров Кыргызской Республики</h4>
            <p>
              720071, Кыргызская Республика,<br>
              город Бишкек, проспект Чуй, 265-а<br>
              <strong>Телефон</strong>+996 998 950 850<br>
              <strong>Почта</strong> okuu@dpa.gov.kg<br>
            </p>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Полезные ссылки</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="#hero">Главная</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#why-us">О нас</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#services">Наши курсы</a></li>
              <li><i class="bx bx-chevron-right"></i><a href="News.php">Новости</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="assets/documents/Privacy_Policy_watermark_watermarked.pdf">Политика конфиденциальности</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="assets/documents/license.pdf">Лицензия</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="assets/documents/position.pdf">Положение</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="assets/documents/ordinance.pdf">Постановление</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="assets/documents/contract_with_government_authorities.docx">Договор с государственными органами </a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Наши курсы</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="courses-cybersecurity.php">Кибербезопасность</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="courses-DigitalLaw.php">Цифровое право</a></li>
              <!-- <li><i class="bx bx-chevron-right"></i> <a href="#">Программирование</a></li> -->
              <li><i class="bx bx-chevron-right"></i> <a href="course-LegalBasisAndProtectionOfPersonalData.php">Правовые основы защиты персональных данных</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="courses-dataAnalysis.php">Аналитика данных</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Мы в социальных сетях</h4>
            <div class="social-links mt-3">

              <a href="https://www.facebook.com/okuudpa.kg" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="https://instagram.com/okuudpa.kg?igshid=MmU2YjMzNjRlOQ==" class="instagram"><i class="bx bxl-instagram"></i></a>
              <a href=" https://t.me/educenterDPA" class="telegram"><i class="bx bxl-telegram"></i></a>

            </div>
          </div>

        </div>
      </div>
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/waypoints/noframework.waypoints.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>