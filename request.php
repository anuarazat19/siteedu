<?php
session_start();
$source = isset($_SESSION['source']) ? $_SESSION['source'] : 'Неизвестно';
?>


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Registration Page</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="assets/vendor/bootstrap/css/bootstrap.css" />
  <!-- =======================================================
  * Template Name: Arsha - v4.10.0
  * Template URL: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>





<body>
  <!-- ======= Header ======= -->
  <header id="header-request-id">
    <div class="container d-flex align-items-center">

      <h1 class="logo me-auto">
        <a href="index.php">
          <h5>
            Учебный центр <br>Государственного агентства по защите персональных данных <br>при Кабинете Министров Кыргызской Республики
          </h5>
        </a>
      </h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo me-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="index.php">Главная</a></li>
          <li><a class="nav-link scrollto" href="index.php">О нас</a></li>
          <li><a class="nav-link scrollto" href="index.php ">Курсы</a></li>
          <li><a class="nav-link scrollto" href="index.php ">Контакты</a></li>
          <li><a class="nav-link" href="News.php">Новости</a></li>
          <li><a href="https://www.facebook.com/okuudpa.kg" class="facebook"><i class="bx bxl-facebook"></i></a></li>
          <li> <a href="https://instagram.com/okuudpa.kg?igshid=MmU2YjMzNjRlOQ==" class="instagram"><i class="bx bxl-instagram"></i></a></li>
          <li> <a href=" https://t.me/educenterDPA" class="telegram"><i class="bx bxl-telegram"></i></a></li>
          <!-- <li><a class="getstarted scrollto" href="#about">Связаться с нами</a></li> -->
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->
    </div>
  </header><!-- End Header -->



  <section id="portfolio-details" class="portfolio-details">
    <div class="container">
      <div class="row col-md-6 col-md-offset-3">
        <div class="panel panel-primary">
          <div class="panel-heading text-center">
            <h1>Получить информацию о курсе</h1>
          </div>
          <div class="panel-body">
            <form action="mail.php" method="post">
              <div class="form-group">
                <label for="Surname">Фамилия</label>
                <input type="text" class="form-control" id="Surname" name="Surname" pattern="[A-Za-zА-Яа-яЁё\s]+" title="Введите только буквы" required/>
              </div>
              <div class="form-group">
                <label for="FirstName">Имя</label>
                <input type="text" class="form-control" id="FirstName" name="FirstName" pattern="[A-Za-zА-Яа-яЁё\s]+" title="Введите только буквы" required/>
              </div>
              <div class="form-group">
                <label for="MiddleName">Отчество</label>
                <input type="text" class="form-control" id="MiddleName" name="MiddleName" pattern="[A-Za-zА-Яа-яЁё\s]+" title="Введите только буквы" required/>
              </div>

              <div class="form-group">
                <label for="Email">Почта</label>
                <input type="email" class="form-control" id="Email" name="Email"  required/>
              </div>

              <div class="form-group">
                <label for="Number">Номер телефона</label>
                <input type="text" class="form-control" id="Number" name="Number"maxlength="10" pattern="[0-9]*" title="Введите только цифры"/>
              </div>
              <div class="form-group">
                <label for="Courses"></label>
                <input type="hidden" class="form-control" id="Courses" name="Courses" value=<?php echo $source; ?> />
              </div>
              
              <button type="submit" class="btn btn-primary" name="send">Отправить</button>
              
              <?php
              if (isset($_SESSION['message'])) {
                echo '<p class = "msg">' . $_SESSION['message'] .       ' </p>';
              }
              unset($_SESSION['message'])
              ?>
            </form>
          </div>

        </div>
      </div>
    </div>
  </section>
  <!-- <div class="toast" role="alert" aria-live="assertive" aria-atomic="true">
    <div class="toast-header">
      <img src="..." class="rounded mr-2" alt="...">
      <strong class="mr-auto">Bootstrap</strong>
      <small>11 mins ago</small>
      <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="toast-body">
      Hello, world! This is a toast message.
    </div>
  </div> -->


  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Учебный центр Государственного агентства по защите персональных данных при Кабинете Министров Кыргызской Республики</h4>
            <p>
              720071, Кыргызская Республика,<br>
              город Бишкек, проспект Чуй, 265-а<br>
              <strong>Телефон</strong>+996 998 950 850<br>
              <strong>Почта</strong> okuu@dpa.gov.kg<br>
            </p>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Полезные ссылки</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="index.php">Главная</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="index.php">О нас</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="index.php">Наши курсы</a></li>
              <li><i class="bx bx-chevron-right"></i><a href="News.php">Новости</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="assets/documents/Privacy_Policy_watermark_watermarked.pdf">Политика конфиденциальности</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="assets/documents/license.pdf">Лицензия</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="assets/documents/position.pdf">Положение</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="assets/documents/ordinance.pdf">Постановление</a></li>
            </ul>
          </div>
          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Наши курсы</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="courses-cybersecurity.php">Кибербезопасность</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="courses-DigitalLaw.php">Цифровое право</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="course-LegalBasisAndProtectionOfPersonalData.php">Правовые основы защиты персональных данных</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="courses-dataAnalysis.php">Аналитика данных</a></li>
            </ul>
          </div>
          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Мы в социальных сетях</h4>
            <div class="social-links mt-3">
              <a href="https://www.facebook.com/okuudpa.kg" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="https://instagram.com/okuudpa.kg?igshid=MmU2YjMzNjRlOQ==" class="instagram"><i class="bx bxl-instagram"></i></a>
              <a href=" https://t.me/educenterDPA" class="telegram"><i class="bx bxl-telegram"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
  <script src="assets/vendor/waypoints/noframework.waypoints.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

  <!-- <script>
    // Функция для установки текущего времени в скрытое поле
    function setCurrentTime() {
      var currentTime = new Date().toLocaleTimeString();
      document.getElementById('current_time').value = currentTime;
    }

    // Вызываем функцию setCurrentTime при загрузке страницы или перед отправкой формы
    window.onload = setCurrentTime;
    document.querySelector('form').addEventListener('submit', setCurrentTime);
  </script> -->
</body>

</html>