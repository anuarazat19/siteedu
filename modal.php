<!-- Modal Edit-->
<div class="modal fade" id="editModal<?= $value['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content shadow">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Редактировать запись № <?= $value['id'] ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="?id=<?= $value['id'] ?>" method="post">
          <div class="form-group">
            <input type="text" class="form-control" name="edit_surname" value="<?= $value['Surname'] ?>" placeholder="Фамилия">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="edit_firstname" value="<?= $value['FirstName'] ?>" placeholder="Имя">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="edit_middlename" value="<?= $value['MiddleName'] ?>" placeholder="Отчество">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="edit_email" value="<?= $value['Email'] ?>" placeholder="Почта">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="edit_number" value="<?= $value['Number'] ?>" placeholder="Номер">
          </div>
          <div class="form-group">
            <input type="text" class="form-control" name="edit_courses" value="<?= $value['Courses'] ?>" placeholder="Курсы">
          </div>
          <div class="form-group">
            <!-- сделать через option -->
            <label for=""> рассмотрено? (1 -  да ,0 - нет)
            <input type="text" class="form-control" name="edit_reviewed" value="<?= $value['Reviewed'] ?>" >
            
            </label>            
          </div>
          <div class="form-group">            
          <input type="date" class="form-control" name="edit_reviewtime" value="<?= $value['ReviewTime'] ?>" >
          </div>
          <div class="modal-footer">
            <button type="submit" name="edit-submit" class="btn btn-primary">Обновить</button>
          </div>
      </div>

    </div>

    </form>
  </div>
</div>
</div>
</div>

<!-- DELETE MODAL -->
<div class="modal fade" id="deleteModal<?= $value['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content shadow">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Удалить запись № <?= $value['id'] ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
        <form action="?id=<?= $value['id'] ?>" method="post">
          <button type="submit" name="delete_submit" class="btn btn-danger">Удалить</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- CHECK MODAL -->
<div class="modal fade" id="checkModal<?= $value['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content shadow">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Рассмотрено? <?= $value['id'] ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
        <form action="?id=<?= $value['id'] ?>" method="post">
          <button type="submit" name="yes_submit" class="btn btn-danger">Да</button>
        </form>
      </div>
    </div>
  </div>
</div>