<?php 
session_start();
if(isset($_SESSION['user']))
{
    header('Location: admin.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Администраторская</title>
    <link rel="stylesheet" href = "assets\css\admin.css">

</head>
<body>
<!--    форма авторизации  -->
    <form action="signin.php" method="post">
        <label > Логин</label>
        <input type="text" name = "login" placeholder=" введите логин">
        <label > Пароль</label>
        <input type="password" name = "password" placeholder=" введите пароль">        
        <button type ="submit">Войти</button>
        <?php
        if(isset($_SESSION['message']))
        {
            echo '<p class = "msg">'. $_SESSION['message']. '</p>';
        }            
         unset($_SESSION['message']) ?>
         
    </form>
</body>
</html>