<?php
session_start();
if (!isset($_SESSION['user'])) {
	header('Location: auth.php');
}
include 'func.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<title>Главная страница</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.css">
	
	<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/css/bootstrap.min.css" rel="stylesheet" /> -->
	<link href="https://cdn.datatables.net/1.13.5/css/dataTables.bootstrap4.min.css" rel="stylesheet" />


</head>

<body>
	<div class="container">
		<div class="headOfPage">
			<form>
			 <center><h2><?= $_SESSION['user']['full_name'] ?></h2>
			 <a href="logout.php" class="logout">Выход</a></center>	
				<button class="btn btn-success mb-1" onclick="openLink()"><i class="fa fa-user-plus"></i></button>
				<a href="#"><?= $_SESSION['user']['email'] ?></a>
				
			</form>
		</div>
		<div class="row">
			<div class="col mt-1">
				<table class="table shadow ">
					<thead class="thead-dark">
						<tr>
							<th>№</th>
							<th>Фамилия</th>
							<th>Имя</th>
							<th>Отчество</th>
							<th>Почта</th>
							<th>Номер</th>
							<th>Курс</th>
							<th>Время отправки</th>
							<th>Рассмотрено?<br>(1 - да,<br> 0 - нет)</th>
							<th>Время рассмотрения</th>
							<th>Действия</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($result as $value) { ?>
							<tr>
								<td><?= $value['id'] ?></td>
								<td><?= $value['Surname'] ?></td>
								<td><?= $value['FirstName'] ?></td>
								<td><?= $value['MiddleName'] ?></td>
								<td><?= $value['Email'] ?></td>
								<td><?= $value['Number'] ?></td>
								<td><?= $value['Courses'] ?></td>
								<td><?= $value['SendingTime'] ?></td>
								<td><?= $value['Reviewed'] ?></td>
								<td><?= $value['ReviewTime'] ?></td>
								<td>
									<a href="?check=<?= $value['id'] ?>" class="btn btn-success btn-sm" data-toggle="modal" data-target="#checkModal<?= $value['id'] ?>">Рассмотрено?</a>
									<a href="?edit=<?= $value['id'] ?>" class="btn btn-success btn-sm" data-toggle="modal" data-target="#editModal<?= $value['id'] ?>"><i class="fa fa-edit"></i></a>
									<a href="?delete=<?= $value['id'] ?>" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deleteModal<?= $value['id'] ?>"><i class="fa fa-trash"></i></a>
									<?php require 'modal.php'; ?>
								</td>
							</tr> <?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<!-- Modal -->
	<!-- <div class="modal fade" tabindex="-1" role="dialog" id="Modal">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content shadow">
	      <div class="modal-header">
	        <h5 class="modal-title">Добавить пользователя</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <form action="" method="post">
	        	<div class="form-group">
	        		<input type="text" class="form-control" name="Surname" value="" placeholder="Фамилия">
	        	</div>
                <div class="form-group">
	        		<input type="text" class="form-control" name="FirstName" value="" placeholder="Имя">
	        	</div>
                <div class="form-group">
	        		<input type="text" class="form-control" name="MiddleName" value="" placeholder="Отчество">
	        	</div>
                <div class="form-group">
	        		<input type="text" class="form-control" name="Email" value="" placeholder="Email">
	        	</div>
                <div class="form-group">
	        		<input type="text" class="form-control" name="Number" value="" placeholder="Номер">
	        	</div>
                <div class="form-group">
	        		<input type="text" class="form-control" name="Courses" value="" placeholder="Курсы">
	        	</div>
	        	
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
	        <button type="submit" name="submit" class="btn btn-primary">Добавить</button>
	      </div>
	  		</form>
	    </div>
	  </div>
	</div> -->



	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	<script src="https://code.jquery.com/jquery-3.7.0.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"></script>
	<script src="https://cdn.datatables.net/1.13.5/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.13.5/js/dataTables.bootstrap4.min.js"></script>
	<script>
		function openLink() {
			window.open('register.php', '_blank');
		}
		$(document).ready(function() {
			$('table').DataTable();
		})
	</script>
</body>

</html>