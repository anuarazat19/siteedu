<?php
session_start();
$_SESSION['source'] = 'Правовые_основы_защиты_персональных_данных';
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Legal Basis And Protection Of Personal Data</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Arsha - v4.10.0
  * Template URL: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-inner-pages">
    <div class="container d-flex align-items-center">

      <h1 class="logo me-auto">
        <a href="index.php">
          <h5>
            Учебный центр <br>Государственного агентства по защите персональных данных <br>при Кабинете Министров Кыргызской Республики
          </h5>
        </a>
      </h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo me-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav id="navbar" class="navbar">
        <ul>
          <!-- <li><a class="nav-link scrollto " href="#hero">Home</a></li>
          <li><a class="nav-link scrollto" href="#about">About</a></li>
          <li><a class="nav-link scrollto" href="#services">Services</a></li>
          <li><a class="nav-link  active scrollto" href="#portfolio">Portfolio</a></li>
          <li><a class="nav-link scrollto" href="#team">Team</a></li>
          <li class="dropdown"><a href="#"><span>Drop Down</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="#">Drop Down 1</a></li>
              <li class="dropdown"><a href="#"><span>Deep Drop Down</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                  <li><a href="#">Deep Drop Down 2</a></li>
                  <li><a href="#">Deep Drop Down 3</a></li>
                  <li><a href="#">Deep Drop Down 4</a></li>
                  <li><a href="#">Deep Drop Down 5</a></li>
                </ul>
              </li>
              <li><a href="#">Drop Down 2</a></li>
              <li><a href="#">Drop Down 3</a></li>
              <li><a href="#">Drop Down 4</a></li>
            </ul>
          </li>
          <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
          <li><a class="getstarted scrollto" href="#about">Get Started</a></li> -->
          <li><a class="nav-link scrollto active" href="index.php">Главная</a></li>
          <li><a class="nav-link scrollto" href="index.php">О нас</a></li>
          <li><a class="nav-link scrollto" href="index.php ">Курсы</a></li>
          <li><a class="nav-link scrollto" href="index.php ">Контакты</a></li>
          <li><a class="nav-link" href="News.php">Новости</a></li>
          <li><a href="https://www.facebook.com/okuudpa.kg" class="facebook"><i class="bx bxl-facebook"></i></a></li>
          <li> <a href="https://instagram.com/okuudpa.kg?igshid=MmU2YjMzNjRlOQ==" class="instagram"><i class="bx bxl-instagram"></i></a></li>
          <li> <a href=" https://t.me/educenterDPA" class="telegram"><i class="bx bxl-telegram"></i></a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="index.php">Главная</a></li>
          <li>Детали курса</li>
        </ol>
        <h2>Детали курса</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
      <div class="container">

        <div class="row gy-4">

          <div class="col-lg-8">
            <div class="portfolio-details-slider swiper">
              <div class="swiper-wrapper align-items-center">

                <div class="swiper-slide">
                  <img src="assets/img/ks.jpg" alt="">
                </div>

                <div class="swiper-slide">
                  <img src="assets/img/ks2.jpg" alt="">
                </div>

                <!-- <div class="swiper-slide">
                  <img src="assets/img/portfolio/portfolio-3.jpg" alt="">
                </div> -->

              </div>
              <div class="swiper-pagination"></div>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="portfolio-info">
              <h4>Правовые основы защиты персональных данных</h4>
              <ul>
                <!-- <li><strong>Начало курса:</strong> 17.07.2023</li> -->
                <li><strong>Продолжительность:</strong> 2 недели</li>
                <li><strong>Максимальное кол-во студентов:</strong>17</li>
                <li><strong>Преподователь</strong>:Клим Омельченко <a href="#"></a></li>
                <li><strong>Стоимость курса</strong>: 9 500с за курс</li>
                <li><strong>График курсов</strong>: 08:00 - 09:30 , 5р. в неделю</li>
                <li><strong>Язык</strong>: русский</li>
              </ul>
            </div>
            <div class="portfolio-description">
              <h2>О чем этот курс</h2>
              <p>
                Изучите особенности сбора, хранения и обработки персональных данных, специфику законодательства Кыргызской Республики и ознакомьтесь с международным опытом. Освоите необходимые условия работы для соответствия с требованиями законодательства о персональных данных
              </p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Portfolio Details Section -->
    <section id="course-program" class="course-program section-bg">
      <div class="container-fluid" data-aos="fade-up">

        <div class="row">

          <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">

            <div class="content">
              <h3>Программа курса <strong> <br>Правовые основы  защиты персональных данных</strong></h3>
              <p>

                <br><strong> Базовые навыки для обучения:</strong>
                <br>- Высшее образование в сфере юриспруденции
                <br>- Студенты старших курсов
                <br>- Юристы и специалисты по информационой и кибербезопасности
                <br>- Все желающие освоить основы защиты ПД
              </p>
            </div>

            <div class="accordion-list">
              <ul>
                <li>
                  <a data-bs-toggle="collapse" class="collapse" data-bs-target="#accordion-list-1"><span>01</span>Введение в правовые основы защиты персональных данных <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-1" class="collapse show" data-bs-parent=".accordion-list">
                    <p>
                      краткое описание
                    </p>
                  </div>
                </li>

                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-2" class="collapsed"><span>02</span>Нормативно-правовые основы защиты персональных данных <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-2" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      краткое описание
                    </p>
                  </div>
                </li>
                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-3" class="collapsed"><span>03</span>Теоретические основы защиты персональных данных<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-3" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      краткое описание
                    </p>
                  </div>
                </li>
                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-4" class="collapsed"><span>04</span>Принципы и условия обработки, хранения, использования и передачи персональных данных<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-4" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      краткое описание
                    </p>
                  </div>
                </li>

                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-5" class="collapsed"><span>05</span>Права субъекта персональных данных<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-5" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      краткое описание
                    </p>
                  </div>
                </li>
                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-6" class="collapsed"><span>06</span>Обязанности оператора по обеспечению безопасности персональных данных при их обработке<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-6" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      краткое описание
                    </p>
                  </div>
                </li>
                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-7" class="collapsed"><span>07</span>Государственный контроль и надзор за сбор, обработкой и хранением персональных данных<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-7" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      краткое описание
                    </p>
                  </div>
                </li>
                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-8" class="collapsed"><span>08</span>Ответственность за нарушение требований Закона КР «Об информации персонального характера»<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-8" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      краткое описание
                    </p>
                  </div>
                </li>
                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-9" class="collapsed"><span>09</span>Международный опыт нормативно-правовой защиты персональных данных<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-9" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      краткое описание
                    </p>
                  </div>
                </li>
                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-10" class="collapsed"><span>10</span>Обеспечение защиты информации в автоматизированных системах<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-10" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      краткое описание
                    </p>
                  </div>
                </li>



              </ul>
            </div>

          </div>
          <!-- <script type='text/javascript'>document.addEventListener('DOMContentLoaded', function () {window.setTimeout(document.querySelector('svg').classList.add('animated'),1000);})</script> -->
          <div class="col-lg-4 align-items-stretch order-1 order-lg-2 img" style='background-image: url("assets/img/cyber_bullying.gif");' data-aos="zoom-in" data-aos-delay="150">&nbsp;
          </div>
          <!-- <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style='background-image: url("assets/img/why-us.png");' data-aos="zoom-in" data-aos-delay="150">&nbsp;</div> -->
        </div>

      </div>

      <!-- Start prof details -->
      <!-- Start section container divider -->
      <!-- </section>
    <section>
      <div class="footer-newsletter">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-6">
              <h4>О преподователе</h4>
              <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
              <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
            </div>
          </div>
        </div>
      </div>
    
    </section> -->

      <!-- end section container divider -->
      <!-- Start section container divider -->
      <section class="about_section layout_padding">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <div class="detail-box">
                <div class="heading_container">
                  <h1>
                    Клим Омельченко
                  </h1>
                </div>
                <p>
                <h5>
                  Эксперт по вопросам цифрового права, информации персонального характера и приватности, член экспертного совета при Госагентстве по защите персональных данных при Кабинете Министров Кыргызской Республики Имеет преподавательский стаж более 10 лет, является практикующим юристом. Является экспертом рабочей группы по разработке Цифрового кодекса Кыргызской Республики. Провёл большое количество исследований проведения исследований в области цифрового права, защиты персональных данных, приватности, кибербезопасности.
Имеются научные статьи по проблемным вопросам нормативных правовых актов в области информации персонального характера.
Имеет сертификаты по кибербезопасности, GDPR, и цифровой трансформации.
                </h5>

                </p>
                <!-- <a href="">
                Read More
              </a> -->
              </div>
            </div>
            <div class="col-md-6">
              <div class="img-box">
                <div class="stripe_design sd1"></div>
                <div class="stripe_design sd2"></div>
                <div class="stripe_design sd3"></div>
                <div class="stripe_design sd4"></div>
                <div class="stripe_design sd5"></div>
                <div class="stripe_design sd6"></div>
                <img src="assets/img/KlimSerg.jpg" alt="" />
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- End prof details -->
  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-newsletter">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-6">
            <h4>Запишитесь прямо сейчас!</h4>
            <br> <a href="rekvisiteLegalBasis.php" target="_blank"><strong>Реквизиты</strong></a>
            <br>- 72 академических часа (5р в неделю )
           
            <center>всего за<strong>
                <h3>9 500 с</h3>
              </strong> </center>
            <br>
            <form action="request.php?source=2" method="post">
              <!-- <a href="request.php?source=4">Записаться</a> -->
              <button class="btn btn-primary">Записаться</button>
            </form>
          </div>
        </div>
      </div>
    </div>




    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Учебный центр Государственного агентства по защите персональных данных при Кабинете Министров Кыргызской Республики</h4>
            <p>
              720071, Кыргызская Республика,<br>
              город Бишкек, проспект Чуй, 265-а<br>
              <strong>Телефон</strong>+996 998 950 850<br>
              <strong>Почта</strong> okuu@dpa.gov.kg<br>
            </p>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Полезные ссылки</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="index.php">Главная</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="index.php">О нас</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="index.php">Наши курсы</a></li>
              <li><i class="bx bx-chevron-right"></i><a href="News.php">Новости</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="assets/documents/Privacy_Policy_watermark_watermarked.pdf">Политика конфиденциальности</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="assets/documents/license.pdf">Лицензия</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="assets/documents/position.pdf">Положение</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="assets/documents/ordinance.pdf">Постановление</a></li>
            </ul>
          </div>
          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Наши курсы</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="courses-cybersecurity.php">Кибербезопасность</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="courses-DigitalLaw.php">Цифровое право</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="course-LegalBasisAndProtectionOfPersonalData.php">Правовые основы защиты персональных данных</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="courses-dataAnalysis.php">Аналитика данных</a></li>
            </ul>
          </div>
          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Мы в социальных сетях</h4>
            <div class="social-links mt-3">
              <a href="https://www.facebook.com/okuudpa.kg" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="https://instagram.com/okuudpa.kg?igshid=MmU2YjMzNjRlOQ==" class="instagram"><i class="bx bxl-instagram"></i></a>
              <a href=" https://t.me/educenterDPA" class="telegram"><i class="bx bxl-telegram"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- 
    <div class="container footer-bottom clearfix">
      <div class="copyright">
        &copy; Copyright <strong><span>Arsha</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
       
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer> -->
    <!-- End Footer -->

    <div id="preloader"></div>
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="assets/vendor/aos/aos.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="assets/vendor/waypoints/noframework.waypoints.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>

    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>

</body>

</html>