<?php
session_start();
$_SESSION['source'] = 'Цифровое_право';
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Новости</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Arsha - v4.10.0
  * Template URL: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-inner-pages">
    <div class="container d-flex align-items-center">

      <h1 class="logo me-auto">
        <a href="index.php">
          <h5>
            Учебный центр <br>Государственного агентства по защите персональных данных <br>при Кабинете Министров Кыргызской Республики
          </h5>
        </a>
      </h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo me-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav id="navbar" class="navbar">
        <ul>
          <!-- <li><a class="nav-link scrollto " href="#hero">Home</a></li>
          <li><a class="nav-link scrollto" href="#about">About</a></li>
          <li><a class="nav-link scrollto" href="#services">Services</a></li>
          <li><a class="nav-link  active scrollto" href="#portfolio">Portfolio</a></li>
          <li><a class="nav-link scrollto" href="#team">Team</a></li>
          <li class="dropdown"><a href="#"><span>Drop Down</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="#">Drop Down 1</a></li>
              <li class="dropdown"><a href="#"><span>Deep Drop Down</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                  <li><a href="#">Deep Drop Down 2</a></li>
                  <li><a href="#">Deep Drop Down 3</a></li>
                  <li><a href="#">Deep Drop Down 4</a></li>
                  <li><a href="#">Deep Drop Down 5</a></li>
                </ul>
              </li>
              <li><a href="#">Drop Down 2</a></li>
              <li><a href="#">Drop Down 3</a></li>
              <li><a href="#">Drop Down 4</a></li>
            </ul>
          </li>
          <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
          <li><a class="getstarted scrollto" href="#about">Get Started</a></li> -->
          <li><a class="nav-link scrollto active" href="index.php">Главная</a></li>
          <li><a class="nav-link scrollto" href="index.php">О нас</a></li>
          <li><a class="nav-link scrollto" href="index.php ">Курсы</a></li>
          <li><a class="nav-link scrollto" href="index.php ">Контакты</a></li>
          <li><a class="nav-link" href="News.php">Новости</a></li>
          <li><a href="https://www.facebook.com/okuudpa.kg" class="facebook"><i class="bx bxl-facebook"></i></a></li>
          <li> <a href="https://instagram.com/okuudpa.kg?igshid=MmU2YjMzNjRlOQ==" class="instagram"><i class="bx bxl-instagram"></i></a></li>
          <li> <a href=" https://t.me/educenterDPA" class="telegram"><i class="bx bxl-telegram"></i></a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="index.php">Главная</a></li>
          <li>Новости</li>
        </ol>
        <h2>Новости</h2>
        <p>



          <br><strong>Объявление о конкурсе на замещение преподавателей курсов в Учебный центр Государственного агентства по защите персональных данных при Кабинете Министров Кыргызской Республики. Учебный центр Государственного агентства по защите персональных данных при Кабинете Министров Кыргызской Республики объявляет конкурс на замещение вакантных мест преподавателей курсов по следующим направлениям: </strong>

          <br>1. Кибербезопасность
          <br>2. Правовые основы защиты персональных данных
          <br>3. Аналитика данных
          <br>4. Цифровые навыки Excel

          <br><strong>Требования к кандидатам:</strong>

          <br><strong>для курса “Кибербезопасность”</strong>
          <br>- Высшее образование по соответствующей специальности;
          <br>- Опыт работы в выбранной области не менее 5 лет;
          <br>- Уверенное владение материалом и практическими навыками в соответствующей области;
          <br>- Коммуникабельность, умение преподавать и передавать знания;
          <br>- Готовность к разработке учебных материалов и проведению практических занятий.

          <br><strong>для курса “Правовые основы защиты персональных данных ”</strong>
          <br>- Высшее образование по соответствующей специальности;
          <br>- Опыт работы в выбранной области не менее 5 лет;
          <br>- Уверенное владение материалом и практическими навыками в соответствующей области;
          <br>- Коммуникабельность, умение преподавать и передавать знания;
          <br>- Готовность к разработке учебных материалов и проведению практических занятий.

          <br><strong>для курса “Аналитика данных”</strong>
          <br>- Высшее образование по соответствующей специальности;
          <br>- Опыт работы в выбранной области не менее 1 года;
          <br>- Уверенное владение материалом и практическими навыками в соответствующей области;
          <br>- Коммуникабельность, умение преподавать и передавать знания;
          <br>- Готовность к разработке учебных материалов и проведению практических занятий.

          <br><strong>для курса “Цифровые навыки - Excel”</strong>
          <br>- Высшее образование по соответствующей специальности;
          <br>- Опыт работы в выбранной области не менее 6 месяцев;
          <br>- Уверенное владение материалом и практическими навыками в соответствующей области;
          <br>- Коммуникабельность, умение преподавать и передавать знания;
          <br>- Готовность к разработке учебных материалов и проведению практических занятий.


          <br><strong>Обязанности преподавателей:</strong>
          <br>- Подготовка и проведение занятий в соответствии с учебными программами;
          <br>- Разработка учебных материалов и заданий;
          <br>- Оценка и контроль успеваемости студентов;
          <br>- Консультирование студентов и ответы на их вопросы.


          <br><strong>Процесс отбора:</strong>
          <br>1. Представление заявки, включающей резюме, сопроводительное письмо и копии документов, подтверждающих образование и опыт работы.
          <br>2. Собеседование с кандидатами, на котором будут проверены знания, профессиональные компетенции и педагогические навыки.
          <br>3. Презентация пробного урока или лекции.


          <br>Заявки принимаются по адресу: г. Бишкек, проспект Чуй, 265а, здание Национальной академии наук, вход со стороны Калыка Акиева, или по электронной почте: <strong>okuu@dpa.gov.kg</strong>.

          <br>Дополнительную информацию можно получить по телефону <strong>+996 998 950 50</strong>.

          <br>Учебный центр Государственного агентства по защите персональных данных при Кабинете Министров Кыргызской Республики оставляет за собой право связаться только с выбранными кандидатами.

          <br>Мы ждем ваших заявок и надеемся на интересное и продуктивное сотрудничество!

        <div id="dateText">
          <p>02/06/2023</p>
        </div>
        </p>
      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Portfolio Details Section ======= -->

    <!-- <section id="course-program" class="course-program section-bg">
      <div class="container-fluid" data-aos="fade-up">

        <div class="row">

          <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">

            <div class="content">
              <h3>Программа курса <strong> <br>Цифровое право</strong></h3>
              
            </div>

            

          </div> -->
    <!-- <script type='text/javascript'>document.addEventListener('DOMContentLoaded', function () {window.setTimeout(document.querySelector('svg').classList.add('animated'),1000);})</script> -->

    <!-- </div> -->
    <!-- <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style='background-image: url("assets/img/why-us.png");' data-aos="zoom-in" data-aos-delay="150">&nbsp;</div> -->
    <!-- </div>

      </div> -->

    <!-- Start prof details -->
    <!-- Start section container divider -->
    <!-- </section>
    <section>
      <div class="footer-newsletter">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-6">
              <h4>О преподователе</h4>
              <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
              <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
            </div>
          </div>
        </div>
      </div>
    
    </section> -->

    <!-- end section container divider -->
    <!-- Start section container divider -->


    <!-- End prof details -->
  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Учебный центр Государственного агентства по защите персональных данных при Кабинете Министров Кыргызской Республики</h4>
            <p>
              720071, Кыргызская Республика,<br>
              город Бишкек, проспект Чуй, 265-а<br>
              <strong>Телефон</strong>+996 998 950 850<br>
              <strong>Почта</strong> okuu@dpa.gov.kg<br>
            </p>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Полезные ссылки</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="index.php">Главная</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="index.php">О нас</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="index.php">Наши курсы</a></li>
              <li><i class="bx bx-chevron-right"></i><a href="News.php">Новости</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="assets/documents/Privacy_Policy_watermark_watermarked.pdf">Политика конфиденциальности</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="assets/documents/license.pdf">Лицензия</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="assets/documents/position.pdf">Положение</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="assets/documents/ordinance.pdf">Постановление</a></li>
            </ul>
          </div>
          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Наши курсы</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="courses-cybersecurity.php">Кибербезопасность</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="courses-DigitalLaw.php">Цифровое право</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="course-LegalBasisAndProtectionOfPersonalData.php">Правовые основы защиты персональных данных</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="courses-dataAnalysis.php">Аналитика данных</a></li>
            </ul>
          </div>
          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Мы в социальных сетях</h4>
            <div class="social-links mt-3">
              <a href="https://www.facebook.com/okuudpa.kg" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="https://instagram.com/okuudpa.kg?igshid=MmU2YjMzNjRlOQ==" class="instagram"><i class="bx bxl-instagram"></i></a>
              <a href=" https://t.me/educenterDPA" class="telegram"><i class="bx bxl-telegram"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- 
    <div class="container footer-bottom clearfix">
      <div class="copyright">
        &copy; Copyright <strong><span>Arsha</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
       
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer> -->
    <!-- End Footer -->

    <div id="preloader"></div>
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="assets/vendor/aos/aos.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="assets/vendor/waypoints/noframework.waypoints.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>

    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>

</body>

</html>