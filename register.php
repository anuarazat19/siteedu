<?php 
session_start();
// if(isset($_SESSION['user']))
// {
//     header('Location: admin.php');
// }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Регистрация</title>
    <link rel="stylesheet" href="assets\css\admin.css">

</head>

<body>
    <!--    форма авторизации  -->
    <form action="signup.php" method="post">
        <label> ФИО</label>
        <input type="text" name="full_name" placeholder="Введите свое полное имя">

        <label>Почта</label>
        <input type="email" name="email" placeholder="Введите адрес своей почты">
        <label>Логин</label>
        <input type="text" name="login" placeholder="Введите логин">
        <label>Пароль</label>
        <input type="password" name="password" placeholder="Введите пароль">
        <label>Подтверждение пароля</label>
        <input type="password" name="password_confirm" placeholder="Подтвердите пароль">
        <button type= "submit" class="btn btn-primary" name = "add"> Добавить </button>
        <?php
        if (isset($_SESSION['message']) ) {
            echo '<p class = "msg">' . $_SESSION['message'] .       ' </p>';
        }
        unset($_SESSION['message'])
        ?>

    </form>
</body>

</html>