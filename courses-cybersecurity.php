<?php
session_start();
$_SESSION['source'] = 'Кибербезопасность';
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Cybersecurity</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Arsha - v4.10.0
  * Template URL: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-inner-pages">
    <div class="container d-flex align-items-center">

      <h1 class="logo me-auto">
        <a href="index.php">
          <h5>
            Учебный центр <br>Государственного агентства по защите персональных данных <br>при Кабинете Министров Кыргызской Республики
          </h5>
        </a>
      </h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo me-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav id="navbar" class="navbar">
        <ul>
          <!-- <li><a class="nav-link scrollto " href="#hero">Home</a></li>
          <li><a class="nav-link scrollto" href="#about">About</a></li>
          <li><a class="nav-link scrollto" href="#services">Services</a></li>
          <li><a class="nav-link  active scrollto" href="#portfolio">Portfolio</a></li>
          <li><a class="nav-link scrollto" href="#team">Team</a></li>
          <li class="dropdown"><a href="#"><span>Drop Down</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="#">Drop Down 1</a></li>
              <li class="dropdown"><a href="#"><span>Deep Drop Down</span> <i class="bi bi-chevron-right"></i></a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                  <li><a href="#">Deep Drop Down 2</a></li>
                  <li><a href="#">Deep Drop Down 3</a></li>
                  <li><a href="#">Deep Drop Down 4</a></li>
                  <li><a href="#">Deep Drop Down 5</a></li>
                </ul>
              </li>
              <li><a href="#">Drop Down 2</a></li>
              <li><a href="#">Drop Down 3</a></li>
              <li><a href="#">Drop Down 4</a></li>
            </ul>
          </li>
          <li><a class="nav-link scrollto" href="#contact">Contact</a></li>
          <li><a class="getstarted scrollto" href="#about">Get Started</a></li> -->
          <li><a class="nav-link scrollto active" href="index.php">Главная</a></li>
          <li><a class="nav-link scrollto" href="index.php">О нас</a></li>
          <li><a class="nav-link scrollto" href="index.php ">Курсы</a></li>
          <li><a class="nav-link scrollto" href="index.php ">Контакты</a></li>
          <li><a class="nav-link" href="News.php">Новости</a></li>
          <li><a href="https://www.facebook.com/okuudpa.kg" class="facebook"><i class="bx bxl-facebook"></i></a></li>
          <li> <a href="https://instagram.com/okuudpa.kg?igshid=MmU2YjMzNjRlOQ==" class="instagram"><i class="bx bxl-instagram"></i></a></li>
          <li> <a href=" https://t.me/educenterDPA" class="telegram"><i class="bx bxl-telegram"></i></a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <main id="main">

    <!-- ======= Breadcrumbs ======= -->
    <section id="breadcrumbs" class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="index.php">Главная</a></li>
          <li>Детали курса</li>
        </ol>
        <h2>Детали курса</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
      <div class="container">

        <div class="row gy-4">

          <div class="col-lg-8">
            <div class="portfolio-details-slider swiper">
              <div class="swiper-wrapper align-items-center">

                <div class="swiper-slide">
                  <img src="assets/img/cyber1.jpg" alt="">
                </div>

                <div class="swiper-slide">
                  <img src="assets/img/cyber2.jpg" alt="">
                </div>

                <!-- <div class="swiper-slide">
                  <img src="assets/img/portfolio/portfolio-3.jpg" alt="">
                </div> -->

              </div>
              <div class="swiper-pagination"></div>
            </div>
          </div>

          <div class="col-lg-4">
            <div class="portfolio-info">
              <h4>Кибербезопасность</h4>
              <ul>
                <!-- <li><strong>Начало курса:</strong> 17.07.2023</li> -->
                <li><strong>Продолжительность:</strong> 1.5 месяца (один уровень)</li>
                <li><strong>Максимальное кол-во студентов:</strong>17</li>
                <li><strong>Преподователь</strong>: Тимур Губаев</li>
                <li><strong>Стоимость курса</strong>: 12 300с за 1 уровень</li>
                <li><strong>График курсов</strong>: 18:30-20:00 , 3р. в неделю</li>
                <li><strong>Язык</strong>: русский</li>
              </ul>
            </div>
            <div class="portfolio-description">
              <h2>О чем этот курс</h2>
              <p>
                Данный курс предлагает комплексное погружение в сферу цифровой безопасности, предоставляя студентам необходимые знания и навыки для эффективного обеспечения безопасности информационных активов. В ходе обучения студенты ознакомятся с основными аспектами безопасности активов, инжиниринга безопасности, коммуникаций и сетевой безопасности, идентификации и управления доступом, оценки и тестирования безопасности, операций безопасности, а также безопасности разработки программного обеспечения. Курс обеспечивает практическую ориентацию, позволяя студентам развивать навыки анализа рисков, разработки безопасных систем, управления уязвимостями, а также эффективного реагирования на инциденты безопасности. Завершив данный курс, студенты будут готовы к успешной работе в области информационной безопасности и смогут активно участвовать в обеспечении защиты информационных ресурсов организаций
              </p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Portfolio Details Section -->
    <section id="course-program" class="course-program section-bg">
      <div class="container-fluid" data-aos="fade-up">

        <div class="row">

          <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">

            <div class="content">
              <h3>Программа курса <strong> <br>Кибербезопасность</strong></h3>
              <p>

                <br><strong>Базовые навыки для обучения:</strong>
                <br><strong>1 уровень:</strong>
                <br> - Студенты, специалисты в области информационных технологий или специалисты смежных областей с навыками знаний системного администрирования
                <br> - те, кто хорошо владеют компьютером
                <br> - работы по базе данных

                <br><strong>2 уровень:</strong>
                <br>- студенты старших курсов, специалисты в области информационных технологий или специалисты смежных областей с навыками знаний системного администрирования
                <br>- знание основы SQL запросов,
                <br>- небольшой опыт в разработках программного обеспечения

                <br><strong>3-4 уровень:</strong>
                <br>- высшее образование в сфере информационных технологий и связей, специалисты в области информационных технологий или специалисты смежных областей с навыками знаний системного администрирования
                <br>- опыт работы в кибербезопасности
                <br>- продвинутый уровень владения компьютером и программами.
              </p>
            </div>

            <div class="accordion-list">
              <ul>
                <li>
                  <a data-bs-toggle="collapse" class="collapse" data-bs-target="#accordion-list-1"><span>1</span> Уровень <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-1" class="collapse show" data-bs-parent=".accordion-list">
                    <p>
                      <br> Концепция конфиденциальности целостности и доступности;
                      <br>Принципы управления безопасностью;
                      <br>Соответствие требованиям;
                      <br>Нормативные вопросы;
                      <br>Профессиональная этика;
                      <br>Политики безопасности стандарты и процедуры
                      <br>Классификация информации и активов;
                      <br>Владение;
                      <br>Защита конфиденциальности;
                      <br>Элементы управления безопасностью данных;
                      <br>Требования к обработке
                      <br>Процессы инжиниринга и проектирование защищенной инфраструктуры;
                      <br>Концептуальные концепции безопасности;
                      <br>Модели оценки безопасности;
                      <br>Возможности безопасности информационных систем;
                      <br>Архитектуры безопасности проектирование и уязвимости элементов решений

                    </p>
                  </div>
                </li>

                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-2" class="collapsed"><span>2</span>Уровень <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-2" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      <br>Уязвимости веб-систем;
                      <br>Уязвимости мобильных систем;
                      <br>Встраиваемые устройства и уязвимости в кибер-физических системах;
                      <br>Криптография;
                      <br>Принципы проектирования безопасности объектов;
                      <br>Физическая охрана
                      <br>Проектирование защищенной сетевой архитектуры;
                      <br>Компоненты сетевой безопасности;
                      <br>Защита каналов связи;
                      <br>Сетевые атаки
                      <br>Управление физическими и логическими ресурсами;
                      <br>Идентификация и аутентификация людей и устройств;
                      <br>Идентификация как сервис;


                    </p>
                  </div>
                </li>

                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-3" class="collapsed"><span>3</span>Уровень<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-3" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      <br>Сторонние службы идентификации;
                      <br>Атаки на управление доступом
                      <br> Жизненный цикл управления доступом
                      <br>Стратегии оценки и тестирования;
                      <br>Данные процесса безопасности;
                      <br>Тестирование контроля безопасности;
                      <br>Обработка данных тестирования;
                      <br>Уязвимости архитектуры безопасности
                      <br>Поддержка расследований и требования;
                      <br>Деятельность по ведению журнала и мониторингу;
                      <br>Выделение ресурсов;
                      <br>Основные концепции операционной безопасности;
                      <br>Методы защиты ресурсов;
                      <br>Управление инцидентами;


                    </p>
                  </div>
                </li>
                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-4" class="collapsed"><span>4</span>Уровень<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-4" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      <br>Превентивные меры
                      <br>Управление патчами и уязвимостями;
                      <br>Процессы управления изменениями;
                      <br>Стратегии восстановления;
                      <br>Процессы и планирование аварийного восстановления
                      <br>Безопасность в жизненном цикле разработки программного обеспечения;
                      <br>Средства контроля безопасности среды разработки;
                      <br>Эффективность обеспечения безопасности программного обеспечения;
                      <br>Влияние безопасности на процессы разработки программного обеспечения
                    </p>
                  </div>
                </li>
              </ul>
            </div>

          </div>
          <!-- <script type='text/javascript'>document.addEventListener('DOMContentLoaded', function () {window.setTimeout(document.querySelector('svg').classList.add('animated'),1000);})</script> -->
          <div class="col-lg-4 align-items-stretch order-1 order-lg-2 img" style='background-image: url("assets/img/TopSecret.gif");' data-aos="zoom-in" data-aos-delay="150">&nbsp;
          </div>
          <!-- <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style='background-image: url("assets/img/why-us.png");' data-aos="zoom-in" data-aos-delay="150">&nbsp;</div> -->
        </div>

      </div>

      <!-- Start prof details -->
      <!-- Start section container divider -->
      <!-- </section>
    <section>
      <div class="footer-newsletter">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-6">
              <h4>О преподователе</h4>
              <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
              <form action="" method="post">
              <input type="email" name="email"><input type="submit" value="Subscribe">
            </form>
            </div>
          </div>
        </div>
      </div>
    
    </section> -->

      <!-- end section container divider -->
      <!-- Start section container divider -->
      <section class="about_section layout_padding">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <div class="detail-box">
                <div class="heading_container">
                  <h1>
                    Тимур Губаев
                  </h1>
                </div>
                <p>
                <h5>
                  Тимур Губаев, преподаватель курса "Кибербезопасность", является независимым экспертом и офицером по кибербезопасности. Он успешно принимал участие в создании нормативно-правовых актов в области кибербезопасности и осуществил множество проектов в коммерческих организациях, связанных с данной тематикой. У него имеется более 10 международных сертификатов в области информационных технологий и информационной безопасности, включая престижный сертификат CISSP.
                </h5>

                </p>
                <!-- <a href="">
                Read More
              </a> -->
              </div>
            </div>
            <div class="col-md-6">
              <div class="img-box">
                <div class="stripe_design sd1"></div>
                <div class="stripe_design sd2"></div>
                <div class="stripe_design sd3"></div>
                <div class="stripe_design sd4"></div>
                <div class="stripe_design sd5"></div>
                <div class="stripe_design sd6"></div>
                <img src="assets/img/Timur.jpg" alt="" />
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- End prof details -->
  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-newsletter">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-6">
            <h4>Запишитесь прямо сейчас!</h4>
            <br><a href="rekvisiteCyberSecurity.php" target="_blank"><strong>Реквизиты</strong></a>
            <br>- 83 академических часа (3р в неделю)
            <center>всего за<strong>
                <h3>12 300 с</h3>
              </strong> </center>
            <br>
            <form action="request.php?source=2" method="post">
              <!-- <a href="request.php?source=4">Записаться</a> -->
              <button class="btn btn-primary">Записаться</button>
            </form>
          </div>
        </div>
      </div>
    </div>




    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>Учебный центр Государственного агентства по защите персональных данных при Кабинете Министров Кыргызской Республики</h4>
            <p>
              720071, Кыргызская Республика,<br>
              город Бишкек, проспект Чуй, 265-а<br>
              <strong>Телефон</strong>+996 998 950 850<br>
              <strong>Почта</strong> okuu@dpa.gov.kg<br>
            </p>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Полезные ссылки</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="index.php">Главная</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="index.php">О нас</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="index.php">Наши курсы</a></li>
              <li><i class="bx bx-chevron-right"></i><a href="News.php">Новости</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="assets/documents/Privacy_Policy_watermark_watermarked.pdf">Политика конфиденциальности</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="assets/documents/license.pdf">Лицензия</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="assets/documents/position.pdf">Положение</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="assets/documents/ordinance.pdf">Постановление</a></li>
            </ul>
          </div>
          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Наши курсы</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="courses-cybersecurity.php">Кибербезопасность</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="courses-DigitalLaw.php">Цифровое право</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="course-LegalBasisAndProtectionOfPersonalData.php">Правовые основы защиты персональных данных</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="courses-dataAnalysis.php">Аналитика данных</a></li>
            </ul>
          </div>
          <div class="col-lg-3 col-md-6 footer-links">
            <h4>Мы в социальных сетях</h4>
            <div class="social-links mt-3">
              <a href="https://www.facebook.com/okuudpa.kg" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="https://instagram.com/okuudpa.kg?igshid=MmU2YjMzNjRlOQ==" class="instagram"><i class="bx bxl-instagram"></i></a>
              <a href=" https://t.me/educenterDPA" class="telegram"><i class="bx bxl-telegram"></i></a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- 
    <div class="container footer-bottom clearfix">
      <div class="copyright">
        &copy; Copyright <strong><span>Arsha</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
       
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer> -->
    <!-- End Footer -->

    <div id="preloader"></div>
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="assets/vendor/aos/aos.js"></script>
    <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="assets/vendor/waypoints/noframework.waypoints.js"></script>
    <script src="assets/vendor/php-email-form/validate.js"></script>

    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>

</body>

</html>